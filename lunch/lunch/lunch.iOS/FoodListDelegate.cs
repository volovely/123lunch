﻿using System;
using UIKit;

namespace lunch.iOS
{
    public class FoodListDelegate : UITableViewDelegate
    {
        public FoodListDelegate()
        {
        }

        public override nfloat GetHeightForRow(UITableView tableView, Foundation.NSIndexPath indexPath)
        {
            return 120;
        }
    }
}
