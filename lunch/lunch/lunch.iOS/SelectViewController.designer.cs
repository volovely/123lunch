// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace lunch.iOS
{
    [Register ("SelectViewController")]
    partial class SelectViewController
    {
        [Action ("MockTouched:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void MockTouched (UIKit.UIButton sender);

        [Action ("RealTouched:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void RealTouched (UIKit.UIButton sender);

        void ReleaseDesignerOutlets ()
        {
        }
    }
}