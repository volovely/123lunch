﻿using System;

using Foundation;
using UIKit;

namespace lunch.iOS
{
    public partial class PlaceDetailNoUrlTableViewCell : UITableViewCell
    {
        public static readonly NSString Key = new NSString("PlaceDetailNoUrlTableViewCell");
        public static readonly UINib Nib;

        static PlaceDetailNoUrlTableViewCell()
        {
            Nib = UINib.FromName("PlaceDetailNoUrlTableViewCell", NSBundle.MainBundle);
        }

        protected PlaceDetailNoUrlTableViewCell(IntPtr handle) : base(handle)
        {
            // Note: this .ctor should not contain any initialization logic.
        }

        public void SetupCell(VisualPlace visualPlace)
        {
            _name.Text = visualPlace.Name;
            _phones.Text = string.Join(", ", visualPlace.Phones);
            _address.Text = visualPlace.Address;
            if (string.IsNullOrEmpty(visualPlace.Description))
            {
                _description.Text = @"


This will actually work, only thing is how i have my forward and back buttons to control the UIWebView is not through my implementation files i do it by right clicking and dragging from the webview to the button and then select the action. What should i use to implement them into my.m file? – user3180303 Jan 12 '14 at 0:30



I'm not sure I understand what you mean. Could you post a screen capture? You need to link your buttons from your .xib file to your class and add an IBOutlet in your .h file so you can access them in your .m file. – Damien Jan 12 '14 at 0:41



Ok on the left is my.h and the right is .m, i.imgur.com / gR3oqwy.png and i have connected the go back and gofoward to the webview – user3180303 Jan 12 '14 at 0:58



Replace goBack and go forward by self.goback and self.goforward and it should be fine. – Damien Jan 12 '14 at 1:03
1

[webView canGoBack] is boolean... – Adam Waite Nov 24 '14 at 19:35
show 7 more comments
up vote
0
down vote



I'm not aware of anything built in to UIWebView that allows this but maybe you could try keeping track of the pages with a variable.

Every time a url request is made the UIWebView delegate method gets called and you could increment the variable there(see here). Then decrement the variable once the user has selected the back button.

Hope that helps.
";
            }
            else
            {
                _description.Text = visualPlace.Description;
            }
            if (string.IsNullOrEmpty(visualPlace.Icon))
            {
                _avatarImageView.Image = UIImage.FromBundle("EmptyImage");
            }
        }
    }
}
