// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace lunch.iOS
{
    [Register ("FoodItemCell")]
    partial class FoodItemCell
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel _foodCategory { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel _foodName { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel _foodPrice { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView _imageView { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (_foodCategory != null) {
                _foodCategory.Dispose ();
                _foodCategory = null;
            }

            if (_foodName != null) {
                _foodName.Dispose ();
                _foodName = null;
            }

            if (_foodPrice != null) {
                _foodPrice.Dispose ();
                _foodPrice = null;
            }

            if (_imageView != null) {
                _imageView.Dispose ();
                _imageView = null;
            }
        }
    }
}