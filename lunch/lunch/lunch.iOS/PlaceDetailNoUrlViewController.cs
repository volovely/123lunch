﻿using System;
using Foundation;
using UIKit;

namespace lunch.iOS
{
    public partial class PlaceDetailNoUrlViewController : UIViewController, IUITableViewDataSource, IUITableViewDelegate
    {
        private VisualPlace _visualPlace;

        public PlaceDetailNoUrlViewController() : base("PlaceDetailNoUrlViewController", null)
        {
        }

        public PlaceDetailNoUrlViewController(VisualPlace visualPlace)
        {
            _visualPlace = visualPlace;
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            // Perform any additional setup after loading the view, typically from a nib.
            _tableView.RegisterNibForCellReuse(PlaceDetailNoUrlTableViewCell.Nib, PlaceDetailNoUrlTableViewCell.Key);
            _tableView.EstimatedRowHeight = _tableView.RowHeight;
            _tableView.RowHeight = UITableView.AutomaticDimension;

            _tableView.DataSource = this;
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }

        public nint RowsInSection(UITableView tableView, nint section)
        {
            return 1;
        }

        public UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            var cell = tableView.DequeueReusableCell(PlaceDetailNoUrlTableViewCell.Key, indexPath) as PlaceDetailNoUrlTableViewCell;
            cell.SetupCell(_visualPlace);
            return cell;
        }
    }
}

