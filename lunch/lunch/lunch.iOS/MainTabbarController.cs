using Foundation;
using System;
using UIKit;

namespace lunch.iOS
{
    public partial class MainTabbarController : UITabBarController
    {
        public MainTabbarController (IntPtr handle) : base (handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            ViewControllers[0].Title = "Карта";
            ViewControllers[0].TabBarItem.Image = UIImage.FromBundle("TabMap");;
            ViewControllers[1].Title = "Список";
            ViewControllers[1].TabBarItem.Image = UIImage.FromBundle("TabList"); ;

            NavigationItem.Title = "123Lunch";
        }

    }
}