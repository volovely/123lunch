using Foundation;
using System;
using UIKit;

namespace lunch.iOS
{
    public partial class SelectViewController : UIViewController
    {
        public SelectViewController (IntPtr handle) : base (handle)
        {
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);
            PosterUsersDal.Users = null;
        }

        partial void MockTouched(UIButton sender)
        {
            PosterUsersDal.State = AppState.Mock;
            GoToMain();
        }

        partial void RealTouched(UIButton sender)
        {
            PosterUsersDal.State = AppState.Real;
            GoToMain();
        }

        private void GoToMain()
        {
            PerformSegue("GoToMain", null);
        }
    }
}