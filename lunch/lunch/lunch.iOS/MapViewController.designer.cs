// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace lunch.iOS
{
    [Register ("MapViewController")]
    partial class MapViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIActivityIndicatorView _activityIndicatorView { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (_activityIndicatorView != null) {
                _activityIndicatorView.Dispose ();
                _activityIndicatorView = null;
            }
        }
    }
}