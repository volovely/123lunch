﻿using System;
using System.Collections.Generic;
using Foundation;
using UIKit;

namespace lunch.iOS
{
    public class FoodListDataSource : UITableViewDataSource
    {
        List<Product> _productList;
        public FoodListDataSource(List<Product> productList)
        {
            _productList = productList;   
        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            var cell = tableView.DequeueReusableCell(FoodItemCell.Key, indexPath) as FoodItemCell;
            cell.SetupCell(_productList[indexPath.Row]);

            return cell;
        }

        public override nint RowsInSection(UITableView tableView, nint section)
        {
            return _productList == null ? 0 : _productList.Count;
        }
    }
}
