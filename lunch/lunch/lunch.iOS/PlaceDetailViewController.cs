﻿using System;

using UIKit;
using Foundation;

namespace lunch.iOS
{
    public partial class PlaceDetailViewController : UIViewController
    {
        private VisualPlace _visualPlace;
        public PlaceDetailViewController() : base("PlaceDetailViewController", null)
        {
        }

        public PlaceDetailViewController(VisualPlace visualPlace)
        {
            _visualPlace = visualPlace;
        }
        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            NavigationItem.Title = _visualPlace.Name;
            _webView.LoadRequest(new NSUrlRequest(NSUrl.FromString(_visualPlace.Url)));

            // Perform any additional setup after loading the view, typically from a nib.
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }
    }
}

