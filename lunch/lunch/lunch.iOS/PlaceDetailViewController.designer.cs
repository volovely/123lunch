// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace lunch.iOS
{
    [Register ("PlaceDetailViewController")]
    partial class PlaceDetailViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIWebView _webView { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (_webView != null) {
                _webView.Dispose ();
                _webView = null;
            }
        }
    }
}