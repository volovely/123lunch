using Foundation;
using System;
using UIKit;
using Google.Maps;
using CoreGraphics;
using System.Collections.Generic;

namespace lunch.iOS
{
    public partial class MapViewController : UIViewController
    {
        private MapView _mapView;
        public MapViewController (IntPtr handle) : base (handle)
        {
        }

        public override void LoadView()
        {
            base.LoadView();
            GetCurrentPosition();
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);
        }

        private async void GetCurrentPosition()
        {
            var position = await CrossLocatorHelper.GetCurrentLocation();
            var camera = CameraPosition.FromCamera(latitude: position.Latitude,
                                                   longitude: position.Longitude,
                                                   zoom: 16);
            _mapView = MapView.FromCamera(CGRect.Empty, camera);
            _mapView.MyLocationEnabled = true;
            _mapView.Settings.MyLocationButton = true;

            View = _mapView;

            //var marker = new Marker();
            //marker.Position = new CoreLocation.CLLocationCoordinate2D(latitude: position.Latitude, longitude: position.Longitude);
            //marker.Icon = UIImage.FromBundle("MeIcon");
            //marker.Map = _mapView;

            InitDataSource();

            _activityIndicatorView.StopAnimating();
        }

        private async void InitDataSource()
        {
            var usersList = await PosterUsersDal.GetPosterUsers();
			var visualPlaceslist = new VisualPlaceModelCreator().GetPlaces(usersList);
            foreach (var item in visualPlaceslist)
            {
                var pos = await CrossLocatorHelper.GetLocationWithAddress(item.Address, "");
                if (pos != null)
                {
                    item.Latitude = pos.Item1;
                    item.Longitude = pos.Item2;
                }
            }

            CreateMarkers(visualPlaceslist);
            _mapView.Delegate = new MyMapViewDelegate(visualPlaceslist, GoToDetailPlace);
		}

        private void CreateMarkers(List<VisualPlace> list)
        {
            int index = 0;
            foreach (var item in list)
            {
				var marker = new Marker();
                marker.Position = new CoreLocation.CLLocationCoordinate2D(latitude: item.Latitude, longitude: item.Longitude);
                marker.Title = item.Name;
                //marker.Snippet = "Australia";
                marker.UserData = new NSString(index.ToString());
				marker.Map = _mapView;

                index++;
			}
        }
        private void GoToDetailPlace(VisualPlace visualPlace)
        {
            //NavigationController?.PushViewController(new PlaceDetailViewController(visualPlace), true);
            NavigationController?.PushViewController(new FoodListViewController(visualPlace.Products), true);
        }
    }
}
