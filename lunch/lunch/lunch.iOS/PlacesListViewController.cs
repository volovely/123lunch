using Foundation;
using System;
using UIKit;

namespace lunch.iOS
{
    public partial class PlacesListViewController : UIViewController
    {
        public PlacesListViewController (IntPtr handle) : base (handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            RegisterNibs();
            _placesCollectionView.Delegate = new PlacesCollectionDelegate();
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);

            InitDataSource();
        }

        private void RegisterNibs()
        {
            _placesCollectionView.RegisterNibForCell(PlaceItemCollectionViewCell.Nib, PlaceItemCollectionViewCell.Key);
        }

        private async void InitDataSource()
        {
            var usersList = await PosterUsersDal.GetPosterUsers();
            var visualPlaceslist = new VisualPlaceModelCreator().GetPlaces(usersList);
            _placesCollectionView.DataSource = new PlacesCollectionDataSource(visualPlaceslist, GoToDetailPlace);
        }

        private void GoToDetailPlace(VisualPlace visualPlace)
        {
            NavigationController?.PushViewController(new FoodListViewController(visualPlace.Products), true);
    //        if(string.IsNullOrEmpty(visualPlace.Url))
    //        {
    //            NavigationController?.PushViewController(new PlaceDetailNoUrlViewController(visualPlace), true);
    //        }
    //        else
    //        {
				//NavigationController?.PushViewController(new PlaceDetailViewController(visualPlace), true);
            //}
        }
    }
}