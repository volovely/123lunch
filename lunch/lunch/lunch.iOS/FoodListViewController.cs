﻿using System;
using System.Collections.Generic;
using UIKit;

namespace lunch.iOS
{
    public partial class FoodListViewController : UIViewController
    {
        private List<Product> _productList;

        public FoodListViewController() : base("FoodListViewController", null)
        {
        }

        public FoodListViewController(List<Product> productList)
        {
            _productList = productList;
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            // Perform any additional setup after loading the view, typically from a nib.
            _tableView.RegisterNibForCellReuse(FoodItemCell.Nib, FoodItemCell.Key);
            _tableView.DataSource = new FoodListDataSource(_productList);
            _tableView.Delegate = new FoodListDelegate();

            if (NavigationItem != null)
            {
                NavigationItem.Title = "Меню";
            }
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }
    }
}

