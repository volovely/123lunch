﻿using CoreGraphics;
using UIKit;

namespace lunch.iOS
{
    public class PlacesCollectionDelegate : UICollectionViewDelegateFlowLayout
    {
        public PlacesCollectionDelegate()
        {
        }

        public override void ItemSelected(UICollectionView collectionView, Foundation.NSIndexPath indexPath)
        {
        }

        public override CoreGraphics.CGSize GetSizeForItem(UICollectionView collectionView, UICollectionViewLayout layout, Foundation.NSIndexPath indexPath)
        {
            return new CGSize(collectionView.Frame.Width, 120);
        }
    }
}
