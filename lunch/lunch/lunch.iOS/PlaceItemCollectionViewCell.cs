﻿using System;
using System.Linq;
using Foundation;
using UIKit;

namespace lunch.iOS
{
    public partial class PlaceItemCollectionViewCell : UICollectionViewCell
    {
        public static readonly NSString Key = new NSString("PlaceItemCollectionViewCell");
        public static readonly UINib Nib;

        private Action<VisualPlace> _actionTap;
        private VisualPlace _visualPlace;

        static PlaceItemCollectionViewCell()
        {
            Nib = UINib.FromName("PlaceItemCollectionViewCell", NSBundle.MainBundle);
        }

        protected PlaceItemCollectionViewCell(IntPtr handle) : base(handle)
        {
            // Note: this .ctor should not contain any initialization logic.
        }

        public override void AwakeFromNib()
        {
            base.AwakeFromNib();

            AddGestureRecognizer(new UITapGestureRecognizer((obj) => 
            { 
                _actionTap?.Invoke(_visualPlace); 
            }));
        }

        public void SetupCell(VisualPlace visualPlace, Action<VisualPlace> action)
        {
            _visualPlace = visualPlace;
            _actionTap = action;

            _name.Text = visualPlace.Name;
            _phone.Text = string.Join(", ", visualPlace.Phones);
            _address.Text = visualPlace.Address;
            if(string.IsNullOrEmpty(visualPlace.Description))
            {
                _description.Text = "---";
            }
            else
            {
                _description.Text = visualPlace.Description;
            }
            if (string.IsNullOrEmpty(visualPlace.Icon))
            {
                _avatarImageView.Image = UIImage.FromBundle("EmptyImage");
            }
            else
            {
                byte[] encodedDataAsBytes = Convert.FromBase64String(visualPlace.Icon);
                NSData data = NSData.FromArray(encodedDataAsBytes);
                var image = UIImage.LoadFromData(data);
                _avatarImageView.Image = image;
            }
        }
    }
}
