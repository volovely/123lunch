// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace lunch.iOS
{
    [Register ("PlaceDetailNoUrlTableViewCell")]
    partial class PlaceDetailNoUrlTableViewCell
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel _address { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView _avatarImageView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel _description { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel _name { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel _phones { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (_address != null) {
                _address.Dispose ();
                _address = null;
            }

            if (_avatarImageView != null) {
                _avatarImageView.Dispose ();
                _avatarImageView = null;
            }

            if (_description != null) {
                _description.Dispose ();
                _description = null;
            }

            if (_name != null) {
                _name.Dispose ();
                _name = null;
            }

            if (_phones != null) {
                _phones.Dispose ();
                _phones = null;
            }
        }
    }
}