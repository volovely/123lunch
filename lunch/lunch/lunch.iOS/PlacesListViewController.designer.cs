// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace lunch.iOS
{
    [Register ("PlacesListViewController")]
    partial class PlacesListViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UICollectionView _placesCollectionView { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (_placesCollectionView != null) {
                _placesCollectionView.Dispose ();
                _placesCollectionView = null;
            }
        }
    }
}