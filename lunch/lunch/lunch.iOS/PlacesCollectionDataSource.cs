﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using Foundation;
using UIKit;

namespace lunch.iOS
{
    public class PlacesCollectionDataSource : UICollectionViewDataSource
    {
        private List<VisualPlace> _users;
        private Action<VisualPlace> _actionTap;
        
        public PlacesCollectionDataSource(List<VisualPlace> users, Action<VisualPlace> actionTap)
        {
            _users = users;
            _actionTap = actionTap;
        }

        public override UICollectionViewCell GetCell(UICollectionView collectionView, NSIndexPath indexPath)
        {
            var cellVisual = collectionView.DequeueReusableCell(PlaceItemCollectionViewCell.Key, indexPath) as PlaceItemCollectionViewCell;
            cellVisual.SetupCell(_users[(int)indexPath.Item], _actionTap);

            return cellVisual;
        }

        public override nint GetItemsCount(UICollectionView collectionView, nint section)
        {
            return _users == null ? 0 : _users.Count;
        }

    }
}
