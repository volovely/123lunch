﻿using System;
using System.Collections.Generic;
using Foundation;
using Google.Maps;

namespace lunch.iOS
{
    public class MyMapViewDelegate : MapViewDelegate
    {
        private List<VisualPlace> _visibleListPlace;
        private Action<VisualPlace> _goToDetailPlace;

        public MyMapViewDelegate(List<VisualPlace> visibleListPlace, Action<VisualPlace> goToDetailPlace)
        {
            _visibleListPlace = visibleListPlace;
            _goToDetailPlace = goToDetailPlace;
        }

        public override UIKit.UIView MarkerInfoWindow(MapView mapView, Marker marker)
        {
            //return base.MarkerInfoWindow(mapView, marker);

            var customInfoWindow = NSBundle.MainBundle.LoadNib(PlaceItemCollectionViewCell.Key, null, null);
            if ((customInfoWindow != null) && (customInfoWindow.Count > 0) && marker.UserData != null)
            {
                var window = customInfoWindow.GetItem<PlaceItemCollectionViewCell>(0);
                int index;
                if (window != null && int.TryParse(marker.UserData.ToString(), out index))
                {
                    window.SetupCell(_visibleListPlace[index], _goToDetailPlace);
                    window.AutosizesSubviews = true;
                    window.Frame = new CoreGraphics.CGRect(0, 0, mapView.Frame.Width * 0.8, 160);
                    window.ContentView.BackgroundColor = UIKit.UIColor.White;
                    window.Layer.CornerRadius = 6;
                    window.UserInteractionEnabled = true;

                    return window;
                }
            }

            return new UIKit.UIView();
        }

        public override void DidTapInfoWindowOfMarker(MapView mapView, Marker marker)
        {
            if (marker.UserData != null)
            {
                if (int.TryParse(marker.UserData.ToString(), out int index))
                {
                    _goToDetailPlace?.Invoke(_visibleListPlace[index]);
                }
            }
        }

    }
}
