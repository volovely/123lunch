﻿using System;

using Foundation;
using UIKit;
using SDWebImage;

namespace lunch.iOS
{
    public partial class FoodItemCell : UITableViewCell
    {
        public static readonly NSString Key = new NSString("FoodItemCell");
        public static readonly UINib Nib;

        static FoodItemCell()
        {
            Nib = UINib.FromName("FoodItemCell", NSBundle.MainBundle);
        }

        protected FoodItemCell(IntPtr handle) : base(handle)
        {
            // Note: this .ctor should not contain any initialization logic.
        }

        public override void AwakeFromNib()
        {
            base.AwakeFromNib();

            var color = UIColor.FromRGB(75, 193, 63);
            //_foodPrice.Layer.BorderColor = color.CGColor;
            //_foodPrice.Layer.BorderWidth = 1;
            _foodPrice.TextColor = color;
            _imageView.Layer.CornerRadius = 6;
            //_foodPrice.Layer.CornerRadius = 2;
        }

        internal void SetupCell(Product product)
        {
            _foodName.Text = product.Name;
            _foodPrice.Text = product.Price.ToString("N2");
            _foodCategory.Text = product.CategoryName;

            if (!string.IsNullOrEmpty(product.PhotoUrl))
            {
                //var data = NSData.FromUrl(NSUrl.FromString(product.PhotoUrl));
                //_imageView.Image = UIImage.LoadFromData(data);
                _imageView.SetImage(NSUrl.FromString(product.PhotoUrl), UIImage.FromBundle("EmptyImage"));
            }
            else
            {
                _imageView.Image = UIImage.FromBundle("EmptyImage");
            }
        }
    }
}
