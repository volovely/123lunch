﻿using System;
using System.Collections.Generic;

namespace lunch
{
    public interface IRepository
    {
        event Action FilteredListChanged;
        void Filter(string condition);
        List<VisualPlace> FilteredList { get; }
    }
}