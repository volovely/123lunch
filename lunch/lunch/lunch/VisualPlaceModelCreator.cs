﻿using System.Collections.Generic;

namespace lunch
{
    public class VisualPlaceModelCreator
    {
        public List<VisualPlace> GetPlaces(List<PosterUser> users)
        {
            var res = new List<VisualPlace>();
            foreach (var item in users)
            {
                foreach (var place in item.Places)
                {
                    res.Add(new VisualPlace(item.Name, place.Name, place.Address, place.Phones, item.Icon, place.Description, place.Url, item.Products));
                }
            }
            return res;
        }
    }
}
