﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace lunch
{
    public class PosterPlace
    {
        [JsonProperty("number")]
        public int Number { get; private set; }
        [JsonProperty("name")]
        public string Name { get; private set; }
        [JsonProperty("address")]
        public string Address { get; private set; }
        [JsonProperty("phones")]
        public string[] Phones { get; private set; }
        [JsonProperty("url")]
        public string Url { get; private set; }
        [JsonProperty("description")]
        public string Description { get; private set; }

        public PosterPlace()
        {
            
        }

        [JsonConstructor]
        public PosterPlace(int number, string name, string address, string[] phones, string url, string description)
        {
            Number = number;
            Name = name;
            Address = address;
            Phones = phones;
            Url = url;
            Description = description;
        }
    }
}
