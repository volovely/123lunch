﻿using System;
using Newtonsoft.Json;

namespace lunch
{
    public class Client
    {
        [JsonConstructor]
        public Client(string account, string token)
        {
            Account = account;
            Token = token;
        }

        public string Account { get; }
        public string Token { get; }
    }
}
