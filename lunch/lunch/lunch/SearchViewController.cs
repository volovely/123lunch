﻿using System;

namespace lunch
{
    public class SearchViewController : ISearchViewController
    {
        private readonly IRepository _repository;
        public SearchViewController(IRepository repository)
        {
            _repository = _repository ?? throw new ArgumentNullException(nameof(_repository));
        }

        public void Search(string condition)
        {
            _repository.Filter(condition);
        }
    }
}
