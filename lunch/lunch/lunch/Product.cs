﻿using System;
using Newtonsoft.Json;

namespace lunch
{
    public class Product
    {
        [JsonConstructor]
        public Product(string name, string categoryName, int categoryId, string photoUrl, double price)
        {
            Name = name;
            CategoryName = categoryName;
            CategoryId = categoryId;
            PhotoUrl = photoUrl;
            Price = price;
        }

        public string Name { get; set; }
        public string CategoryName { get; set; }
        public int CategoryId { get; set; }
        public string PhotoUrl { get; set; }
        public double Price { get; set; }
    }
}
