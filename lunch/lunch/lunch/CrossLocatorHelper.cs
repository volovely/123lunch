﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Plugin.Geolocator;
using Plugin.Geolocator.Abstractions;

namespace lunch
{
    public class CrossLocatorHelper
    {
        public CrossLocatorHelper()
        {
        }

        public const string ApiKey = "AIzaSyD4Cl3PLyLu0YLsmYlkHBK_yRV0Yfjzblk";

        public static async Task<Position> GetCurrentLocation()
        {
            Position position = null;
            try
            {
                var locator = CrossGeolocator.Current;
                locator.DesiredAccuracy = 100;

                position = await locator.GetLastKnownLocationAsync();

                if (position != null)
                {
                    //got a cahched position, so let's use it.
                    return position;
                }

                if (!locator.IsGeolocationAvailable || !locator.IsGeolocationEnabled)
                {
                    //not available or enabled
                    return new Position{
                        Latitude = 0.0,
                        Longitude = 0.0
                    };
                }

                position = await locator.GetPositionAsync(TimeSpan.FromSeconds(20), null, true);

            }
            catch (Exception ex)
            {
                //Display error as we have timed out or can't get location.
            }

            var output = string.Format("Time: {0} \nLat: {1} \nLong: {2} \nAltitude: {3} \nAltitude Accuracy: {4} \nAccuracy: {5} \nHeading: {6} \nSpeed: {7}",
                position.Timestamp, position.Latitude, position.Longitude,
                position.Altitude, position.AltitudeAccuracy, position.Accuracy, position.Heading, position.Speed);

            Debug.WriteLine(output);

            return position;
        }

        public static async Task<Tuple<double, double>> GetLocationWithAddress(string address, string name)
        {
            Tuple<double, double> position = null;
            using (var client = new HttpClient())
            {
                try
                {

                    var resp = await client.GetAsync($"https://maps.googleapis.com/maps/api/geocode/json?address={address}&key={ApiKey}");
                    var content = await resp.Content.ReadAsStringAsync();
                    var location = JsonConvert.DeserializeObject<AddressResponse>(content);
                    if (location?.results.Count > 0)
                    {
                        //got a cahched position, so let's use it.
                        position = new Tuple<double, double>(location.results[0].geometry.location.lat, location.results[0].geometry.location.lng);
                    }


                    //positionList = await locator.GetPositionAsync(TimeSpan.FromSeconds(20), null, true);

                }
                catch (Exception ex)
                {
                    //Display error as we have timed out or can't get location.
                }

            }
            return position;
        }


    }
}
