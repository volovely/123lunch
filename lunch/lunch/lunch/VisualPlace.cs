﻿using System;
using System.Collections.Generic;
using System.Text;

namespace lunch
{
    public class VisualPlace
    {
        public VisualPlace(string account, string name, string address, string[] phones, string icon, string description, string url, List<Product> products)
        {
            Account = account;
            Name = name;
            Address = address;
            Phones = phones;
            Icon = icon;
            Description = description;
            Url = url;
            Products = products;
        }

        public string Account { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string[] Phones { get; set; }
        public string Icon { get; set; }
        public string Description { get; set; }
        public string Url { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public List<Product> Products { get; set; }
    }
}
