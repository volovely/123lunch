﻿using System;
using Newtonsoft.Json;

namespace lunch
{

    public class IconResponse
    {
        [JsonConstructor]
        public IconResponse(ResponseForIcon response)
        {
            this.response = response;
        }

        public ResponseForIcon response { get; set; }

    }


    public class ResponseForIcon
    {
        [JsonConstructor]
        public ResponseForIcon(string value)
        {
            this.value = value;
        }

        public string value { get; set; }
    }

}
