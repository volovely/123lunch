﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace lunch
{
    public class Repository : IRepository
    {
        private readonly List<VisualPlace> _placeList;
        public event Action FilteredListChanged;

        public Repository(List<VisualPlace> placeList)
        {
            _placeList = placeList ?? throw new ArgumentNullException(nameof(placeList));
            FilteredList = placeList;
        }

        public void Filter(string condition)
        {
            if (condition == null)
            {
                condition = string.Empty;
            }
            var c = condition.Replace(" ", ".");
            var result = new List<VisualPlace>();
            var punctuation = c.Where(ch => !char.IsLetter(ch) && !char.IsNumber(ch)).Distinct().ToArray();
            var words = c.Split().Select(x => x.Split(punctuation)).ToList()[0];

            foreach (var place in _placeList)
            {
                var enteringInName = true;
                var enteringInAddress = true;
                foreach (var word in words)
                {
                    int findNamePos = place.Name.ToLower().IndexOf(word.ToLower(), StringComparison.Ordinal);
                    enteringInName &= findNamePos != -1;

                    int findNameAddress = place.Address.ToLower().IndexOf(word.ToLower(), StringComparison.Ordinal);
                    enteringInAddress &= findNameAddress != -1;
                }

                if (enteringInAddress || enteringInName)
                {
                    result.Add(place);
                }
            }

            FilteredList = result;
            FilteredListChanged?.Invoke();
        }

        public List<VisualPlace> FilteredList { get; private set; }
    }
}