﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace lunch
{
    public class Storage
    {
        [JsonConstructor]
        public Storage(int storage_id, string storage_name, string storage_adress)
        {
            this.storage_id = storage_id;
            this.storage_name = storage_name;
            this.storage_adress = storage_adress;
        }

        public int storage_id { get; set; }
        public string storage_name { get; set; }
        public string storage_adress { get; set; }
    }

    public class Response
    {
        [JsonConstructor]
        public Response(string spot_id, string spot_name, string spot_adress, List<Storage> storages)
        {
            this.spot_id = spot_id;
            this.spot_name = spot_name;
            this.spot_adress = spot_adress;
            this.storages = storages;
        }

        public string spot_id { get; set; }
        public string spot_name { get; set; }
        public string spot_adress { get; set; }
        public List<Storage> storages { get; set; }
    }

    public class PosterSpot
    {
        [JsonConstructor]
        public PosterSpot(List<Response> response)
        {
            this.response = response;
        }

        public List<Response> response { get; set; }
    }
}
