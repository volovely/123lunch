﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace lunch
{
    public class PosterUser
    {
        [JsonProperty("id")]
        public string Id { get; private set; }
        [JsonProperty("api_key")]
        public string ApiKey { get; private set; }
        [JsonProperty("name")]
        public string Name { get; private set; }
        [JsonProperty("icon")]
        public string Icon { get; private set; }
        [JsonProperty("places")]
        public PosterPlace[] Places { get; private set; }
        [JsonProperty("products")]
        public List<Product> Products { get; set; }

        public PosterUser()
        {
            
        }

        [JsonConstructor]
        public PosterUser(string id, string api_key, string name, string icon, PosterPlace[] places, List<Product> products)
        {
            Id = id;
            ApiKey = api_key;
            Name = name;
            Icon = icon;
            Places = places;
            Products = products;
        }
    }
}
