﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Views;
using Android.Webkit;
using Android.Widget;
using Toolbar = Android.Support.V7.Widget.Toolbar;

namespace lunch.Droid
{
    [Activity]
    public class WebViewActivity : AppCompatActivity
    {
        private WebView _webView;
        private string _url;
        private string _title;
        private PlaceWebViewClient _webViewClient;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.webview_activity);
            
            var extras = Intent.Extras;
            _url = "";
            _title = "";
            if (extras != null)
            {
                _title = extras.GetString("title");
                _url = extras.GetString("url");
            }
            InitToolbar();
            InitWebView();
        }

        private void InitWebView()
        {
            _webView = FindViewById<WebView>(Resource.Id.detail_webView);
            _webView.Settings.JavaScriptEnabled = true;
            _webView.Settings.DomStorageEnabled = true;
            _webView.Settings.BuiltInZoomControls = true;

            _webViewClient = new PlaceWebViewClient();
            _webView.SetWebViewClient(_webViewClient);

            _webView.LoadUrl(_url);
        }

        private void InitToolbar()
        {
            var toolbar = (Toolbar)FindViewById(Resource.Id.toolbarMain);
            SetSupportActionBar(toolbar);
            SupportActionBar.SetDisplayShowTitleEnabled(false);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);

            var title = FindViewById<TextView>(Resource.Id.toolbar_title);
            title.Text = _title;
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    OnBackPressed();
                    return true;
                default:
                    return base.OnOptionsItemSelected(item);
            }
        }

        public override void OnBackPressed()
        {
            if (_webView.CanGoBack())
            {
                _webView.GoBack();
            }
            else
            {
                base.OnBackPressed();
            }
        }

        private class PlaceWebViewClient : WebViewClient
        {
            public override void OnPageFinished(WebView view, string url)
            {
            }
        }
    }
}