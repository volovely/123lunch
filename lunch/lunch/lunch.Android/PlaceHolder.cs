﻿using System;
using Android.Runtime;
using Android.Support.V4.Content;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using Java.Lang;

namespace lunch.Droid
{
    public class PlaceHolder : RecyclerView.ViewHolder
    {
        private readonly PlaceView _view;

        public PlaceHolder(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
        {
        }

        public PlaceHolder(View itemView) : base(itemView)
        {
            _view = (PlaceView)itemView;
        }

        public void FillCard(VisualPlace item)
        {
            _view.FillCard(item);
        }
    }
}