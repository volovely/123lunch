﻿using System.Collections.Generic;
using System.Linq;
using Android.App;
using Android.Graphics;
using Android.OS;
using Android.Support.V7.App;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using Newtonsoft.Json;
using Toolbar = Android.Support.V7.Widget.Toolbar;

namespace lunch.Droid
{
    [Activity]
    public class DetailActivity : AppCompatActivity
    {
        private RecyclerView _recycler;
        private DishListAdapter _adapter;
        private List<Product> _productlist;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.detail_activity);

            var extras = Intent.Extras;
            VisualPlace places = null;
            if (extras != null)
            {
                var str = extras.GetString("item");
                if (!string.IsNullOrEmpty(str))
                {
                    places = JsonConvert.DeserializeObject<VisualPlace>(str);
                }
            }
            _productlist = places?.Products;
            InitToolbar();

            _recycler = (RecyclerView)FindViewById(Resource.Id.recycler_view);
            _recycler.SetBackgroundColor(Color.LightGray);
        }
        
        protected override void OnResume()
        {
            base.OnResume();
            _adapter = new DishListAdapter(_productlist);
            _recycler.SetAdapter(_adapter);
            _recycler.SetLayoutManager(new LinearLayoutManager(this, (int)Orientation.Vertical, false));
        }

        private void InitToolbar()
        {
            var toolbar = (Toolbar)FindViewById(Resource.Id.toolbarMain);
            SetSupportActionBar(toolbar);
            SupportActionBar.SetDisplayShowTitleEnabled(false);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);

            var title = FindViewById<TextView>(Resource.Id.toolbar_title);
            title.Gravity = GravityFlags.Left;
            title.Text = "Меню";
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.Home:
                    OnBackPressed();
                    return true;
                default:
                    return base.OnOptionsItemSelected(item);
            }
        }
    }
}