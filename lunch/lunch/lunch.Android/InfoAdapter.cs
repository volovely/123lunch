﻿using System;
using System.Collections.Generic;
using Android.Gms.Maps;
using Android.Gms.Maps.Model;
using Android.Views;
using Android.Widget;

namespace lunch.Droid
{
    public class InfoAdapter : Java.Lang.Object, GoogleMap.IInfoWindowAdapter
    {
        private List<VisualPlace> _list;
        private PlaceViewMap _view;

        public InfoAdapter(List<VisualPlace> list, PlaceViewMap view)
        {
            _list = list;
            _view = view;
        }

        public View GetInfoContents(Marker marker)
        {
            int.TryParse(marker.Tag.ToString(), out int i);
            if (i == 0)
            {
                return null;
            }
            _view.FillCard(_list[i - 1]);
            return _view;
        }

        public View GetInfoWindow(Marker marker)
        {
            int.TryParse(marker.Tag.ToString(), out int i);
            if (i == 0)
            {
                return null;
            }
            _view.FillCard(_list[i - 1]);
             
            return _view;
        }
    }
}