﻿using System;
using Android.Content;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.Runtime;
using Android.Support.V4.Content;
using Android.Support.V7.Widget;
using Android.Util;
using Android.Widget;
using Bumptech.Glide;

namespace lunch.Droid
{
    [Register("lunch.Droid.DishView")]
    public class DishView : CardView
    {
        private LinearLayout _rootLayout;
        private ImageView _icon;
        private TextView _title;
        private TextView _categoryName;
        private TextView _price;

        protected DishView(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
        {
        }

        public DishView(Context context) : base(context)
        {
        }

        public DishView(Context context, IAttributeSet attrs) : base(context, attrs)
        {
        }

        public DishView(Context context, IAttributeSet attrs, int defStyleAttr) : base(context, attrs, defStyleAttr)
        {
        }

        private void Init()
        {
            _rootLayout = FindViewById<LinearLayout>(Resource.Id.dish_layout);
            _title = FindViewById<TextView>(Resource.Id.dish_data_title);
            _categoryName = FindViewById<TextView>(Resource.Id.dish_data_cat_name);
            _price = FindViewById<TextView>(Resource.Id.dish_price);
            _icon = FindViewById<ImageView>(Resource.Id.dish_icon);

            _rootLayout.SetBackgroundColor(Color.White);
            _title.SetTextColor(Color.Black);
            _categoryName.SetTextColor(Color.Black);
            _price.SetTextColor(Color.Rgb(75, 193, 63));
        }

        public void FillCard(Product product)
        {
            Init();

            _title.Text = product.Name;
            _categoryName.Text = product.CategoryName;
            _price.Text = product.Price.ToString();
            //_price.Background.Mutate();
            //var drawable = (GradientDrawable)_price.Background;
            //drawable.SetStroke(2, Color.Rgb(75, 193, 63));

            if (string.IsNullOrEmpty(product.PhotoUrl))
            {
                _icon.SetImageDrawable(ContextCompat.GetDrawable(Context, Resource.Mipmap.ic_image));
            }
            else
            {
                Glide.With(Context).Load(product.PhotoUrl).Into(_icon);
            }
        }
    }
}