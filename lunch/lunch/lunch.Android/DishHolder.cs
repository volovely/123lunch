﻿using System;
using Android.Runtime;
using Android.Support.V7.Widget;
using Android.Views;

namespace lunch.Droid
{
    public class DishHolder : RecyclerView.ViewHolder
    {
        private readonly DishView _view;

        public DishHolder(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
        {
        }

        public DishHolder(View itemView) : base(itemView)
        {
            _view = (DishView)itemView;
        }

        public void FillCard(Product product)
        {
            _view.FillCard(product);
        }
    }
}