﻿using System;
using Android.Support.Design.Widget;
using Android.Support.V4.View;

namespace lunch.Droid
{
    public class TabSelectedListener : TabLayout.IOnTabSelectedListener
    {
        private ViewPager _viewPager;

        public TabSelectedListener(ViewPager viewPager)
        {
            _viewPager = viewPager;
        }

        public void Dispose()
        {
        }

        public IntPtr Handle { get; }
        public void OnTabReselected(TabLayout.Tab tab)
        {
        }

        public void OnTabSelected(TabLayout.Tab tab)
        {
            _viewPager.CurrentItem = tab.Position;
        }

        public void OnTabUnselected(TabLayout.Tab tab)
        {
        }
    }
}