﻿using Android.Gms.Maps;
using Android.OS;
using Android.Views;
using Fragment = Android.Support.V4.App.Fragment;

namespace lunch.Droid
{
    public class FrMaps : SupportMapFragment, IOnMapReadyCallback
    {
        MapView _mapView;
        private GoogleMap _map;

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View rootView = inflater.Inflate(Resource.Layout.fr_maps, container, false);

            _mapView = rootView.FindViewById<MapView>(Resource.Id.mapView);
            _mapView.OnCreate(savedInstanceState);
            _mapView.GetMapAsync(this);

            return rootView;
        }

        public void OnMapReady(GoogleMap googleMap)
        {
            _map = googleMap;
            //GoogleMapOptions mapOptions = new GoogleMapOptions()
            //    .InvokeMapType(GoogleMap.MapTypeNormal)
            //    .InvokeZoomControlsEnabled(false)
            //    .InvokeCompassEnabled(true);
            _map.MapType = GoogleMap.MapTypeNormal;
        }

        public override void OnStart()
        {
            base.OnStart();
            _mapView.OnStart();
        }

        public override void OnResume()
        {
            base.OnResume();
            _mapView.OnResume();
        }
        
        public override void OnPause()
        {
            base.OnPause();
            _mapView.OnPause();
        }

        public override void OnStop()
        {
            base.OnStop();
            _mapView.OnStop();
        }

        public override void OnDestroy()
        {
            base.OnDestroy();
            _mapView.OnDestroy();
        }

        public override void OnLowMemory()
        {
            base.OnLowMemory();
            _mapView.OnLowMemory();
        }
    }
}