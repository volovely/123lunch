﻿using System;
using System.Collections.Generic;
using System.Xml;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Gms.Maps;
using Android.Gms.Maps.Model;
using Android.Graphics;
using Android.Widget;
using Android.OS;
using Android.Support.Design.Widget;
using Android.Support.V4.App;
using Android.Support.V4.View;
using Android.Support.V7.App;
using Android.Views;
using Newtonsoft.Json;
using Toolbar = Android.Support.V7.Widget.Toolbar;
using Fragment = Android.Support.V4.App.Fragment;
using FragmentTransaction = Android.Support.V4.App.FragmentTransaction;

namespace lunch.Droid
{
    [Activity(Label = "lunch", Icon = "@mipmap/icon", ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : AppCompatActivity, IOnMapReadyCallback, GoogleMap.IOnInfoWindowClickListener
    {
        private GoogleMap _map;
        private VisualPlaceModelCreator _creator = new VisualPlaceModelCreator();
        private List<PosterUser> _users;

        protected override void OnCreate(Bundle savedState)
        {
            base.OnCreate(savedState);

            SetContentView(Resource.Layout.Main);
        }

        protected override void OnResume()
        {
            base.OnResume();
            InitToolbar();
            GetData();
        }

        private async void GetData()
        {
            _users = await PosterUsersDal.GetPosterUsers();
            InitTabs(_users);
        }

        private void InitTabs(List<PosterUser> users)
        {
            TabLayout tabLayout = FindViewById<TabLayout>(Resource.Id.tab_layout);
            ViewPager viewpager = FindViewById<ViewPager>(Resource.Id.view_pager);

            tabLayout.SetupWithViewPager(viewpager);
            tabLayout.SetBackgroundColor(Resources.GetColor(Resource.Color.colorPrimary, Application.Theme));
            ViewPagerAdapter adapter = new ViewPagerAdapter(SupportFragmentManager, _creator.GetPlaces(users), this);

            adapter.AddFragment("карта");
            adapter.AddFragment("список");
            viewpager.Adapter = adapter;
        }

        private void InitToolbar()
        {
            var toolbar = (Toolbar)FindViewById(Resource.Id.toolbarMain);
            SetSupportActionBar(toolbar);
            SupportActionBar.SetDisplayShowTitleEnabled(false);
            SupportActionBar.SetDisplayHomeAsUpEnabled(false);

            var title = FindViewById<TextView>(Resource.Id.toolbar_title);
            title.Text = "123Lunch";
        }

        public void OnMapReady(GoogleMap googleMap)
        {
            _map = googleMap;
            SetupMap();
        }
        
        private void SetupMap()
        {
            var list = _creator.GetPlaces(_users);
            var map = _map;
            map.MyLocationEnabled = true;
            PlaceViewMap view = (PlaceViewMap)LayoutInflater.Inflate(Resource.Layout.PlaceCardMap, null);
            map.SetInfoWindowAdapter(new InfoAdapter(list, view));
            map.SetOnInfoWindowClickListener(this);
            GetPosition(map);

            CreateMarkers(list);
        }

        private async void GetPosition(GoogleMap map)
        {
            var position = await CrossLocatorHelper.GetCurrentLocation();
            var location = new LatLng(position.Latitude, position.Longitude);
            CameraPosition.Builder builder = CameraPosition.InvokeBuilder();
            builder.Target(location);
            builder.Zoom(16);
            CameraPosition cameraPosition = builder.Build();
            CameraUpdate cameraUpdate = CameraUpdateFactory.NewCameraPosition(cameraPosition);
            map.MoveCamera(cameraUpdate);
            //map.AddMarker(new MarkerOptions()
            //    .SetPosition(location)
            //    .SetIcon(BitmapDescriptorFactory.FromResource(Resource.Mipmap.ic_person_map)));
        }

        private async void CreateMarkers(List<VisualPlace> list)
        {
            for (int i = 0; i < list.Count; i++)
            {
                var item = list[i];
                var pos = await CrossLocatorHelper.GetLocationWithAddress(item.Address, item.Name);
                if (pos != null)
                {
                    item.Latitude = pos.Item1;
                    item.Longitude = pos.Item2;
                }

                var marker = _map.AddMarker(new MarkerOptions()
                    .SetPosition(new LatLng(item.Latitude, item.Longitude)));

                marker.Tag = new Java.Lang.String((i + 1).ToString());
            }
        }

        public void OnInfoWindowClick(Marker marker)
        {
            var list = _creator.GetPlaces(_users);
            int.TryParse(marker.Tag.ToString(), out int index);
            for (int i = 0; i < list.Count; i++)
            {
                var item = list[i];
                if (i == index - 1)
                {
                    Intent intent = new Intent(this, typeof(DetailActivity));

                    string val = JsonConvert.SerializeObject(item);
                    intent.PutExtra("item", val);
                    this.StartActivity(intent);
                }
            }
        }
    }
}

