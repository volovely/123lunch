﻿using System;
using System.Collections.Generic;
using System.Linq;
using Android.Gms.Maps;
using Android.Runtime;
using Android.Support.V4.App;
using Java.Lang;

namespace lunch.Droid
{
    public class ViewPagerAdapter : FragmentStatePagerAdapter
    {
        private List<string> _titleList = new List<string>();

        private List<VisualPlace> placeData;
        private readonly IOnMapReadyCallback _mapListener;


        public ViewPagerAdapter(FragmentManager fm, List<VisualPlace> list, IOnMapReadyCallback mapListener) : base(fm)
        {
            placeData = list;
            _mapListener = mapListener;
        }

        public void AddFragment(string title)
        {
            _titleList.Add(title);
        }

        public override int Count
        {
            get => _titleList.Count;
        }

        public override ICharSequence GetPageTitleFormatted(int position)
        {
            ICharSequence s = new Java.Lang.String(_titleList.ElementAt(position));
            return s;
        }

        public override Fragment GetItem(int position)
        {
            switch (position)
            {
                case 0:
                    {
                        GoogleMapOptions mapOptions = new GoogleMapOptions()
                       .InvokeMapType(GoogleMap.MapTypeNormal)
                       .InvokeZoomControlsEnabled(false)
                       .InvokeCompassEnabled(true);

                        var mapFr = SupportMapFragment.NewInstance(mapOptions);
                        mapFr.GetMapAsync(_mapListener);

                        return mapFr;
                    }
                case 1:
                    {
                        return FrList.NewInstance(placeData);
                    }
                default:
                    {
                        throw new IndexOutOfRangeException("only 2 positions");
                    }
            }
        }
    }
}