﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Support.V4.Content;
using Android.Support.V7.Widget;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace lunch.Droid
{
    [Register("lunch.Droid.PlaceViewMap")]
    public class PlaceViewMap : CardView
    {
        private LinearLayout _rootLayout;
        private TextView _title;
        private TextView _phones;
        private TextView _address;
        private TextView _description;
        private ImageView _imgInfo;
        private ImageView _icon;

        protected PlaceViewMap(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
        {
        }

        public PlaceViewMap(Context context) : base(context)
        {
        }

        public PlaceViewMap(Context context, IAttributeSet attrs) : base(context, attrs)
        {
        }

        public PlaceViewMap(Context context, IAttributeSet attrs, int defStyleAttr) : base(context, attrs, defStyleAttr)
        {
        }

        private void Init()
        {
            _rootLayout = FindViewById<LinearLayout>(Resource.Id.place_layout);
            _title = FindViewById<TextView>(Resource.Id.place_data_title);
            _phones = FindViewById<TextView>(Resource.Id.place_data_phone);
            _address = FindViewById<TextView>(Resource.Id.place_data_address);
            _description = FindViewById<TextView>(Resource.Id.place_data_description);
            _imgInfo = FindViewById<ImageView>(Resource.Id.place_info);
            _icon = FindViewById<ImageView>(Resource.Id.place_icon);

            _rootLayout.SetBackgroundColor(Color.White);
            _title.SetTextColor(Color.Black);
            _phones.SetTextColor(Color.Black);
            _address.SetTextColor(Color.Black);
            _description.SetTextColor(Color.Black);
        }

        public void FillCard(VisualPlace item)
        {
            Init();

            _title.Text = item.Name;
            _address.Text = item.Address;
            _description.Text = item.Description;

            _imgInfo.SetImageDrawable(ContextCompat.GetDrawable(Context, Resource.Mipmap.ic_info));
            if (string.IsNullOrEmpty(item.Icon))
            {
                _icon.SetImageDrawable(ContextCompat.GetDrawable(Context, Resource.Mipmap.ic_image));
            }
            else
            {
                //Glide.With(Context).Load(item.Icon).Into(_icon);

                byte[] imageAsBytes = Base64.Decode(item.Icon, Base64.Default);
                _icon.SetImageBitmap(BitmapFactory.DecodeByteArray(imageAsBytes, 0, imageAsBytes.Length));
            }

            if (!_rootLayout.HasOnClickListeners)
            {
                _rootLayout.Click += (sender, args) =>
                {
                    Intent intent = new Intent(Context, typeof(DetailActivity));
                    (Context as Activity)?.StartActivity(intent);
                };
            }
        }
    }
}