﻿using System.Collections.Generic;
using Android.Support.V7.Widget;
using Android.Views;

namespace lunch.Droid
{
    public class PlaceListAdapter : RecyclerView.Adapter
    {
        private readonly List<VisualPlace> _result;

        public PlaceListAdapter(List<VisualPlace> result)
        {
            _result = result;
        }

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            var item = _result?[position];
            if (!(holder is PlaceHolder vh) || _result == null)
            {
                return;
            }

            vh.FillCard(item);
        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            return new PlaceHolder(LayoutInflater.From(parent.Context).Inflate(Resource.Layout.PlaceCard, parent, false));
        }

        public override int ItemCount => _result?.Count ?? 0;
    }
}