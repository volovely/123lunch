using System.Collections.Generic;
using Android.Graphics;
using Android.OS;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using Newtonsoft.Json;
using Fragment = Android.Support.V4.App.Fragment;

namespace lunch.Droid
{
    public class FrList : Fragment
    {
        private List<VisualPlace> _result;
        private RecyclerView _recycler;
        private PlaceListAdapter _adapter;

        public FrList()
        {
        }
        
        public FrList(List<VisualPlace> result)
        {
            _result = result;
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedState)
        {
            if (container == null)
            {
                return null;
            }
            
            var fragment = (RelativeLayout)View.Inflate(container.Context, Resource.Layout.fr_list, null);
            _recycler = (RecyclerView)fragment.FindViewById(Resource.Id.recycler_view);

            if (Arguments != null && Arguments.ContainsKey("places"))
            {
                List<VisualPlace> tmpList = new List<VisualPlace>();
                foreach (var s in Arguments.GetStringArray("places"))
                {
                    tmpList.Add(JsonConvert.DeserializeObject<VisualPlace>(s));
                }
                _result = tmpList;
            }
            
            _recycler.SetBackgroundColor(Color.LightGray);
            return fragment;
        }

        public override void OnResume()
        {
            base.OnResume();
            _adapter = new PlaceListAdapter(_result);
            _recycler.SetAdapter(_adapter);
            _recycler.SetLayoutManager(new LinearLayoutManager(Context, (int)Orientation.Vertical, false));
        }

        public static FrList NewInstance(List<VisualPlace> places)
        {
            FrList fragment = new FrList();

            Bundle args = new Bundle();
            string[] saveList = new string[places.Count];
            for (int i = 0; i < places.Count; i++)
            {
                saveList[i] = JsonConvert.SerializeObject(places[i]);
            }
            args.PutStringArray("places", saveList);
            fragment.Arguments = args;

            return fragment;
        }
    }
}