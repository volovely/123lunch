﻿using System;
using Android.Content;
using Android.Runtime;
using Android.Util;
using FloatingSearchViews;

namespace lunch.Droid
{
    [Register("mob.Droid.Viper.FilterSearchView.FilterSearchView")]
    public class FilterSearchView : FloatingSearchView
    {
        public FilterSearchView(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
        {
        }

        public FilterSearchView(Context context) : base(context)
        {
            Init();
        }

        public FilterSearchView(Context context, IAttributeSet attrs) : base(context, attrs)
        {
            Init();
        }

        private void Init()
        {
            QueryChange += (sender, args) =>
            {
                ClearSuggestions();
                string str = args.NewQuery;
                Controller?.Search(str);
            };

            ClearSearchAction += (sender, args) =>
            {
                ClearSuggestions();
                Controller?.Search(null);
            };
        }

        public ISearchViewController Controller { get; set; }
    }
}