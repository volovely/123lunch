﻿using System.Collections.Generic;
using Android.Support.V7.Widget;
using Android.Views;

namespace lunch.Droid
{
    public class DishListAdapter : RecyclerView.Adapter
    {
        private readonly List<Product> _productList;

        public DishListAdapter(List<Product> productList)
        {
            _productList = productList;
        }

        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            var item = _productList?[position];
            if (!(holder is DishHolder vh) || _productList == null)
            {
                return;
            }

            vh.FillCard(item);
        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            return new DishHolder(LayoutInflater.From(parent.Context).Inflate(Resource.Layout.DishCard, parent, false));
        }

        public override int ItemCount => _productList?.Count ?? 0;
    }
}